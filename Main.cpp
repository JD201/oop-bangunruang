#include <iostream>
#include <string>
#include <cmath>
#include "Silinder.h" 
// pada bagian ini saya merujuk ke 1 file h saja
// hal ini dikarenakan apabila saya merujuk ke banyak file h, terjadi error(bertabrakan prototipe yang sama)

using namespace std;





int main(int argc, char const *argv[])
{
    Kubus* kubus1 = new Kubus(6);
    kubus1->display();
    cout << "Volume kubus: " << kubus1->getVolume() << " dan Luas permukaan:" << kubus1->getArea() << endl;
    cout << "Diagonal bidang: " << kubus1->getDiagonalBidang() << endl;
    cout << "Diagonal ruang: " << kubus1->getDiagonalRuang() << endl;
    delete kubus1;
    
    
    Balok* balok1 = new Balok(2,3,4);
    balok1->display();
    cout << "Volume balok: " << balok1->getVolume() << " dan Luas permukaan:" << balok1->getArea() << endl;
    cout << "Diagonal ruang: " << balok1->getDiagonalRuang() << endl;
    
    Silinder* silinder1 = new Silinder(2,5);
    silinder1->display();
    cout << "Volume silinder: " << silinder1->getVolume() << " dan Luas permukaan:" << silinder1->getArea() << endl;
    cout << "Keliling alas silinder: " << silinder1->getKelilingAlas() << endl;

        
    
    return 0;
}


