#include <cmath>
#include "BangunRuang.h"



class Kubus : public BangunRuang::BangunRuang{
    private:
    double PanjangRusuk;

    public:
    //Kontruktor kelas kubus
    Kubus(double pR);
    //method 1
    double getDiagonalBidang();
    //method 2
    double getDiagonalRuang();
};