#ifndef __KUBUS
#define __KUBUS
#include<string>
#include <iostream>
#include <cmath>
#include "Kubus.h"

Kubus::Kubus(double pR) : BangunRuang(6, 12, "Kubus",pow(pR,2),pR,4*pR) { 
        this->PanjangRusuk = pR;
    }

double Kubus::getDiagonalBidang(){
        return PanjangRusuk * sqrt(2);
    }

double Kubus::getDiagonalRuang(){
        return PanjangRusuk * sqrt(3);


    }

#endif