#include "Balok.h"
#include <string>
#include <cmath>

Balok::Balok(double p, double l, double t) : BangunRuang::BangunRuang(6, 12, "Balok",p*l,t,2*(p+l)) {
        this->Panjang = p;
        this->Lebar = l;
        this->Tinggi = t; 
    }

double Balok::getDiagonalRuang(){
        return sqrt(pow(Panjang,2) + pow(Lebar,2) + pow(Tinggi,2));
    }