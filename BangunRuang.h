//Definisikan prototipe
#include <string>

using namespace std;


class BangunRuang{
    public:
    int JumlahSisi;
    int JumlahRusuk;
    string NamaBangun;

    double LuasAlas;
    double Tinggi;
    double KelilingAlas;
    
    //Kontruktor
    BangunRuang(int JS, int JR, string name, double la, double t, double kel);
    //Fungsi getVolume
    virtual double getVolume();
    //Fungsi getArea
    virtual double getArea();
    // Getter(memperoleh informasi sesuai konstruktor bangun ruang)
    virtual void display();

    
};