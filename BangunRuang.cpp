#ifndef __BANGUNRUANG
#define __BANGUNRUANG
#include <iostream>
#include<string>
#include"BangunRuang.h"


using namespace std;

BangunRuang::BangunRuang(int JS, int JR, string name, double la, double t, double kel){
        this->JumlahSisi = JS;
        this->JumlahRusuk = JR;
        this->NamaBangun = name;

        this->LuasAlas = la;
        this->Tinggi = t;
        this->KelilingAlas = kel;
    }


double BangunRuang::getVolume(){
    return LuasAlas * Tinggi;
    }

double BangunRuang::getArea(){
    return KelilingAlas* Tinggi + 2*LuasAlas;
    }

void BangunRuang::display(){
    cout << "\nNama bangun: " << this->NamaBangun <<endl;
    cout << "Jumlah sisi: " << this->JumlahSisi <<endl;
    cout << "Jumlah rusuk: " << this->JumlahRusuk <<endl;
    cout << "Luas alas: " << this->LuasAlas <<endl;
    cout << "Tinggi: " << this->Tinggi <<endl;
    cout << "Keliling Alas: " << this->KelilingAlas <<endl;

}

#endif